const topping = [
      {
        "gambar" : "image/buble.png",
        "judul" : "pearls",
        "harga" : "5.000"
      },
      {
        "gambar" : "image/coco.jpg",
        "judul" : "Coconut Jelly",
        "harga" : "5.000"
      },
      {
        "gambar" : "image/jelly.jpg",
        "judul" : "Rainbow Jelly",
        "harga" : "5.000"
      },
      {
        "gambar" : "image/vera.png",
        "judul" : "Aloe vera",
        "harga" : "5.000" 
      }
    ];

    const body = document.querySelector("body");
    for(let menu of topping){
      body.innerHTML += `<div class="row" style="float: left;">
      <div class="col mb-4" >
          <div class="card m-2" style="width: 15rem;">
  <img src="${menu.gambar}" class="card-img-top" alt="...">
  <div class="card-body bg-light">
    <h5 class="card-title">${menu.judul}</h5>
    <p><b>Rp.${menu.harga}</b></p><br>
    <i class="fas fa-star text-success"></i>
    <i class="fas fa-star text-success"></i>
    <i class="fas fa-star text-success"></i>
    <i class="fas fa-star text-success"></i><br>
    <a href="#" class="btn btn-primary" data-target="#menu1" data-toggle="modal">Detail</a>
    <a class="btn btn-danger">Beli Sekarang</a>
  </div>
</div>
</div>
</div>
</div>
`;
}