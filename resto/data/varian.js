const varian =
[{
    "gambar" : "image/matcha.png",
    "judul" : "Matcha Tea Latte",
    "harga" : "24.000"
  },
  {
    "gambar" : "image/tea.png",
    "judul" : "Thai Tea Original",
    "harga" : "22.000"
  },
  {
    "gambar" : "image/taiwan.png",
    "judul" : "Taiwan Plum Iced Tea",
    "harga" : "23.000", 
  },
  {
    "gambar" : "image/taro.png",
    "judul" : "Taro Milk Tea",
    "harga" : "25.000",
  },
  {
    "gambar" : "image/lemon.png",
    "judul" : "Honey Juicy Lemon",
    "harga" : "23.000", 
  }
];


const body = document.querySelector("body");
      for(let menu of varian){
        body.innerHTML += ` <div class="row" style="float: left;">
                                    <div class="col mb-4" >
                                        <div class="card m-2" style="width: 15rem;">
                                <img src="${menu.gambar}" class="card-img-top" alt="...">
                                <div class="card-body bg-light">
                                  <h5 class="card-title">${menu.judul}</h5>
                                  <p><b>Rp.${menu.harga}</b></p><br>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i>
                                  <i class="fas fa-star text-success"></i><br>
                                  <a href="#" class="btn btn-primary" data-target="#menu1" data-toggle="modal">Detail</a>
                                  <a class="btn btn-danger">Beli Sekarang</a>
                                </div>
                              </div>
                            </div>
                            </div>
                            </div>
                              `;
                            }